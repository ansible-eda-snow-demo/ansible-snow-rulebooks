FROM registry.access.redhat.com/ubi9-minimal

RUN microdnf install java-17 python3 gcc python3-devel -y && \
    microdnf clean all && \
    python -m ensurepip --upgrade && \
    pip3 install ansible ansible-rulebook==0.10.1 asyncio aiokafka aiohttp aiosignal

# Exposed port 5000 for altermanager plugin
EXPOSE 5000/tcp
 
ENV HOME="/tmp/ansible-eda"
ENV JAVA_HOME="/usr/lib/jvm/jre-17"

WORKDIR /tmp/collections

COPY collections .

ARG ANSIBLE_EDA_COLLECTION="ansible_ssa-eda-1.0.2.tar.gz"
ARG ANSIBLE_CONTROLLER_COLLECTION="ansible-controller-4.3.0.tar.gz"
ARG ANSIBLE_ITSM_COLLECTION="servicenow-itsm-2.1.0.tar.gz"

RUN ansible-galaxy collection install $ANSIBLE_EDA_COLLECTION $ANSIBLE_CONTROLLER_COLLECTION $ANSIBLE_ITSM_COLLECTION --collections-path "/tmp/ansible-eda"
RUN mkdir -p /usr/share/ansible && ln -s /tmp/ansible-eda /usr/share/ansible/collections

WORKDIR /tmp/ansible-eda

COPY ansible.cfg .
COPY inventory .
COPY rulebooks .
COPY playbooks .

RUN chgrp -R 0 /tmp/ansible-eda && \
    chmod -R g=u /tmp/ansible-eda

CMD ansible-rulebook --rulebook snow-rulebook.yml -i inventory --env-vars SN_HOST,SN_USERNAME,SN_PASSWORD,CONTROLLER_HOST,CONTROLLER_OAUTH_TOKEN --debug
